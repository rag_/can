import sys
import os

import warnings
from image import *

from model import CANNet

from utils import save_checkpoint

import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import datasets, transforms

import numpy as np
import argparse
import json
import cv2
import dataset
import time
import pandas as pd

huber_loss = torch.nn.SmoothL1Loss(reduction='sum')

def loss_quantile(q_low, q_high, y_low, y_mid, y_high, target): 
      
    e1 = (target-y_low)
    l1 = torch.sum(torch.max(q_low*e1, (q_low-1)*e1))
    l2 = huber_loss(y_mid, target)
    e3 = (target-y_high)
    l3 = torch.sum(torch.max(q_high*e3, (q_high-1)*e3))
    
    return l1+l2+l3
    
df_train = pd.DataFrame(columns = ["iter","loss"]) 
df_val = pd.DataFrame(columns = ["epoch", "loss_val", "loss_train", "metrics"])

parser = argparse.ArgumentParser(description='PyTorch CANNet')

parser.add_argument('train_json', metavar='TRAIN', default='../Context-Aware-Crowd-Counting_data/UCF-QNRF_ECCV18/Train/data.json',
                    help='path to train json')
parser.add_argument('val_json', metavar='VAL',
                    help='path to val json', default='../Context-Aware-Crowd-Counting_data/UCF-QNRF_ECCV18/Test/data.json')

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

crop_size = 512

q_low = 0.08
q_high = 0.92

def main():

    global args,best_prec1

    best_prec1 = 1e6

    args = parser.parse_args()
    args.lr = 1e-4
    args.batch_size    = 6
    args.decay         = 5*1e-4
    args.start_epoch   = 0
    args.epochs = 1000
    args.workers = 4
    args.seed = int(time.time())
    args.print_freq = 4
    
    if torch.cuda.is_available():
        torch.cuda.manual_seed(args.seed)


    
    with open(args.train_json, 'r') as outfile:
        train_list = json.load(outfile)
    with open(args.val_json, 'r') as outfile:
        val_list = json.load(outfile) 

    model = CANNet()

    model = model.to(device)

    #criterion = nn.MSELoss(size_average=False).to(device)
    criterion = loss_quantile

    optimizer = torch.optim.Adam(model.parameters(), args.lr,
                                    weight_decay=args.decay)

    for epoch in range(args.start_epoch, args.epochs):
        
        loss_train = train(train_list, model, criterion, optimizer, epoch)
        df_train.to_csv('../logs/train_train.csv', index=False)
        
        prec1, loss_val = validate(val_list, model, criterion)
        
        #loss_train = 0
        df_val.loc[epoch]= [epoch, loss_val, loss_train, prec1]
        df_val.to_csv('../logs/train_val.csv', index = False)

        is_best = (prec1 + loss_val) < best_prec1
        best_prec1 = min(prec1 +loss_val , best_prec1)
        print(' * best MAE {mae:.3f} '.format(mae=best_prec1))

        save_checkpoint({
            'state_dict': model.state_dict(),
        }, is_best)

def train(train_list, model, criterion, optimizer, epoch):

    losses = AverageMeter()
    batch_time = AverageMeter()
    data_time = AverageMeter()
    valid_time = AverageMeter()

    train_loader = torch.utils.data.DataLoader(
        dataset.listDataset(train_list,
                       shuffle=True,
                       transform=transforms.Compose([
                       transforms.ToTensor(),transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225]),
                   ]),

                       train=True,
                       seen=model.seen,
                       batch_size=args.batch_size,
                       num_workers=args.workers,                        
                       crop_size= crop_size
                       ),
        batch_size=args.batch_size)
    print('epoch %d, processed %d samples, lr %.10f' % (epoch, epoch * len(train_loader.dataset), args.lr))

    model.train()
    end = time.time()

    for i,(img, target) in enumerate(train_loader):
        data_time.update(time.time() - end)

        img = img.to(device)
        img = Variable(img)
        y_low, y_mid, y_high = model(img)
        y_mid = y_mid[:,0,:,:]
        y_low = y_low[:,0,:,:]
        y_high = y_high[:,0,:,:]

        target = target.type(torch.FloatTensor).to(device)
        target = Variable(target)

        loss = criterion(q_low ,q_high, y_low, y_mid, y_high, target)
        
        idx = epoch*len(train_loader)+i        
        df_train.loc[idx]= [idx+1,loss.item()]

        losses.update(loss.item(), img.size(0))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  .format(
                   epoch, i, len(train_loader), batch_time=batch_time,
                   data_time=data_time, loss=losses))
    return losses.avg

def validate(val_list, model, criterion):
    print ('begin val')
    losses = AverageMeter()
    val_loader = torch.utils.data.DataLoader(
    dataset.listDataset(val_list,
                   shuffle=False,
                   transform=transforms.Compose([
                       transforms.ToTensor(),transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225]),
                   ]),  
                   train=False
                   ),
    batch_size = 1)

    model.eval()

    mae = 0
    i = 0

    end = time.time()
    for i,(img, target) in enumerate(val_loader):

        start_calc = time.time()
        pred_sum_low, pred_sum_mid, pred_sum_hidh, gt, loss_chunk = predict_amount(img, model, device, target, criterion, q_low, q_high)
        losses.update(sum(loss_chunk), img.size(0))
        #print("val:{i:d}  dt:{dt:.3f} mae:{mae:.3f}  loss:{loss:.3f}  gt:{gt:.3f}".format(i = i , dt = (time.time()-start_calc),mae=abs(pred_sum_mid - target.sum()), loss = sum(loss_chunk), gt = gt))
        mae += abs(pred_sum_mid - target.sum())

    mae = mae/len(val_loader)
    print(" time val: ",time.time()-end)
    print(' * MAE {mae:.3f} '.format(mae=mae))

    return mae.item(), losses.avg

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

if __name__ == '__main__':
    main()
