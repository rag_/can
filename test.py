import h5py
import PIL.Image as Image
import numpy as np
import pandas as pd
import os
import glob
import scipy
from image import *
from model import CANNet
import torch
from torch.autograd import Variable

from sklearn.metrics import mean_squared_error,mean_absolute_error

from torchvision import transforms

from utils import xrange

from timeit import default_timer as timer

crop_size=512;

from pandas import DataFrame


transform=transforms.Compose([
                       transforms.ToTensor(),transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225]),
                   ])

# the folder contains all the test images
img_folder='../Context-Aware-Crowd-Counting_data/UCF-QNRF_ECCV18/Test/image'
img_paths=[]

for img_path in glob.glob(os.path.join(img_folder, '*.jpg')):
    img_paths.append(img_path)

model = CANNet()

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

model.to(device)

checkpoint = torch.load('model_best.pth.tar', map_location = device )

model.load_state_dict(checkpoint['state_dict'])

model.eval()

pred = []
pred_high = []
gt = []
total = 0
negative = 0

df = DataFrame(columns = ["low","mid","high","gt"])
for i in xrange(len(img_paths)):

    gt_file = h5py.File(img_paths[i].replace('.jpg','.h5').replace('image','ground_truth'),'r')
    groundtruth = np.asarray(gt_file['density'])
    gt_sum = np.sum(groundtruth)
    #if gt_sum<1000:
    #    continue
    
    img = transform(Image.open(img_paths[i]).convert('RGB')).to(device)
    img = img.unsqueeze(0)
    pred_sum_low, pred_sum_mid, pred_sum_high, gt_stub, loss_stub= predict_amount(img, model, device)
    
    #pure_name = os.path.splitext(os.path.basename(img_paths[i]))[0]

    
     
    pred.append(pred_sum_mid)
    pred_high.append(pred_sum_high)
    gt.append(gt_sum)

    total += 1
    if (gt[-1]>pred_sum_low and gt[-1]<pred_sum_high):
        negative +=1

    print(img_paths[i].split("/")[-1], "pred:",pred_sum_low, " ", pred_sum_mid," ", pred_sum_high, "gt:",gt[-1], "part", negative/total)

    df.loc[i] = [pred_sum_low, pred_sum_mid, pred_sum_high, gt[-1]]
    
    df.to_csv('../logs/test.csv', index=False) 
    

print(total, negative, negative/total)

if len(pred)>0 and len(gt)>0:
    mae = mean_absolute_error(pred,gt)
    rmse = np.sqrt(mean_squared_error(pred,gt))
    print('MAE: ',mae)
    print('RMSE: ',rmse)
    mae_high = mean_absolute_error(pred_high,gt)
    rmse_high = np.sqrt(mean_squared_error(pred_high,gt))
    print('MAE-HIGH: ',mae_high)
    print('RMSE-HIGH: ',rmse_high)
