import random
import os
import torch
import torch
from torch.autograd import Variable
from PIL import Image
import numpy as np
import h5py
import cv2

def load_data(img_path, train = True, log_delta_file =  None):
    gt_path = img_path.replace('.jpg','.h5').replace('image','ground_truth')
    img = Image.open(img_path).convert('RGB')
    gt_file = h5py.File(gt_path)
    target = np.asarray(gt_file['density'])
    if train:
        ratio = 0.5
        crop_size = (int(img.size[0]*ratio),int(img.size[1]*ratio))
        rdn_value = random.random()
        if rdn_value<0.25:
            dx = 0
            dy = 0
        elif rdn_value<0.5:
            dx = int(img.size[0]*ratio)
            dy = 0
        elif rdn_value<0.75:
            dx = 0
            dy = int(img.size[1]*ratio)
        else:
            dx = int(img.size[0]*ratio)
            dy = int(img.size[1]*ratio)

        img = img.crop((dx,dy,crop_size[0]+dx,crop_size[1]+dy))
        target = target[dy:(crop_size[1]+dy),dx:(crop_size[0]+dx)]
        if random.random()>0.8:
            target = np.fliplr(target)
            img = img.transpose(Image.FLIP_LEFT_RIGHT)
    t1 = np.sum(target)    
    target = cv2.resize(target,(target.shape[1]/8,target.shape[0]/8),interpolation = cv2.INTER_CUBIC)*64
    t2 = np.sum(target)
    if log_delta_file:
        log_delta_file.write("{}\n".format(t1-t2))

    return img,target

def predict_amount(img, model, device, target = None, criterion = None, q_low = None, q_high = None):

    crop_size = 512
    crop_size_t = 64
    
    h,w = img.shape[2:4]
    h_idx = int(np.floor(h/crop_size+1))
    w_idx = int(np.floor(w/crop_size+1))
    
    if criterion:
        h_t, w_t = target.shape[1:3]
        h_t_idx = int(np.floor(h_t/crop_size_t+1))
        w_t_idx = int(np.floor(w_t/crop_size_t+1))
    

    pred_sum_low = 0
    pred_sum_mid = 0
    pred_sum_high = 0 
    losses = []
    
    #print('shape',img.shape)
    #print('target', target.shape)
    gt = 0;
    for i in range(h_idx):
        for j in range(w_idx):
            
            x0 = i*crop_size
            y0 = j*crop_size
            x1 = (i+1)*crop_size
            y1 = (j+1)*crop_size
            
            x0_t = i*crop_size_t
            y0_t = j*crop_size_t
            x1_t = (i+1)*crop_size_t
            y1_t = (j+1)*crop_size_t
            
            if x1>w or y1>h:
                tmp1 = torch.zeros([1, 3, crop_size, crop_size])
                tmp2 = img[:,:,y0:min(y1,h),x0:min(x1,w)]                
                tmp1[:, :, :tmp2.shape[2], :tmp2.shape[3]] = tmp2[:, :, :, :]
                img_crop =  Variable(tmp1.to(device))
                if criterion:
                    t_tmp1 = torch.zeros([1, int(crop_size_t), int(crop_size_t)])
                    t_tmp2 = target[:,y0_t:min(y1_t,h_t),x0:min(x1_t,w_t)]
                    t_tmp1[:, :t_tmp2.shape[1], :t_tmp2.shape[2]] = t_tmp2[:, :, :]
                    target_crop =  Variable(t_tmp1.to(device)).float()
            else:
                img_crop = Variable(img[:,:,y0:y1,x0:x1].to(device))
                if criterion:
                    target_crop = Variable(target[:,y0_t:y1_t,x0_t:x1_t].to(device)).float()
            y_low, y_mid, y_high = model(img_crop)
            if criterion:
                loss = criterion(q_low ,q_high, y_low, y_mid, y_high, target_crop)
                losses.append(loss.item())
            density_crop_low = y_low.data.cpu().numpy()
            density_crop_mid = y_mid.data.cpu().numpy()
            density_crop_high = y_high.data.cpu().numpy()
            pred_sum_low += density_crop_low.sum()
            pred_sum_mid += density_crop_mid.sum()
            pred_sum_high += density_crop_high.sum()
            if criterion:
                 gt += target_crop.cpu().numpy().sum()
    return pred_sum_low, pred_sum_mid, pred_sum_high, gt, losses

def load_data_2(img_path, crop_size, train = True):
    gt_path = img_path.replace('.jpg','.h5').replace('image','ground_truth')
    img = Image.open(img_path).convert('RGB')
    gt_file = h5py.File(gt_path)
    target = np.asarray(gt_file['density'])
    
    
    if train:
        
        h_1 = target.shape[0]-crop_size
        w_1 = target.shape[1]-crop_size
        h_step = np.floor(h_1/crop_size+1)
        w_step = np.floor(w_1/crop_size+1)
   
        y = np.random.randint(max(h_step,1))*crop_size
        x = np.random.randint(max(w_step,1))*crop_size
       
        img = img.crop((y,x,y+crop_size,x+crop_size))
        if y+crop_size > target.shape[0] or x + crop_size > target.shape[1]:
            tmp_1 = np.zeros((crop_size, crop_size), dtype=float)
            tmp_2 = target[y:min(crop_size+y,target.shape[0]),x:min(crop_size+x,target.shape[1])]
            tmp_1[:tmp_2.shape[0],:tmp_2.shape[1]] = tmp_2
            target = tmp_1
        else:   
            target = target[y:(crop_size+y),x:(crop_size+x)]
                   
    t = 8    
    target = cv2.resize(target,(int(target.shape[1]/t), int(target.shape[0]/t)),interpolation = cv2.INTER_CUBIC)*t*t
         
    return img,target