import  h5py
import  scipy.io as io
import PIL.Image as Image
import numpy as np
import os
import glob
from matplotlib import pyplot as plt
from scipy.ndimage.filters import gaussian_filter
from matplotlib import cm as CM
from image import *

# root is the path to ShanghaiTech dataset
root='../Context-Aware-Crowd-Counting_data'

path_data_set = 'UCF-QNRF_ECCV18'
 
part_B_train = os.path.join(root, path_data_set, 'Train')
part_B_test = os.path.join(root, path_data_set, 'Test')
path_sets = [part_B_test, part_B_train]

compression_opts = 3

print(path_sets)

img_paths  = []
for path in path_sets:
    for img_path in glob.glob(os.path.join(path, '*.jpg')):
        img_paths.append(img_path)

for  img_path  in img_paths:
    print(img_path)
    if os.path.exists(img_path.replace('.jpg',".h5")):
        continue
    mat = io.loadmat(img_path.replace('.jpg','_ann.mat').replace('images','ground-truth').replace('IMG_','GT_IMG_'))
    img= plt.imread(img_path)
    k = np.zeros((img.shape[0],img.shape[1]))
    gt = mat["annPoints"]
    for i in range(0,len(gt)):
        if int(gt[i][1])<img.shape[0] and int(gt[i][0])<img.shape[1]:
            k[int(gt[i][1]),int(gt[i][0])]=1
    k = gaussian_filter(k,15)
    with h5py.File(img_path.replace('.jpg','.h5').replace('images','ground-truth'), 'w') as hf:
        hf.create_dataset('density', data=k, compression="gzip", compression_opts=compression_opts)
            
