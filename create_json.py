import json
from os.path import join
import glob


if __name__ == '__main__':
    # path to folder that contains images
    dirs = ["Test", "Train"]
    for dir in dirs:
        img_folder = '../Context-Aware-Crowd-Counting_data/UCF-QNRF_ECCV18/'+dir+'/image'

        # path to the final json file
        output_json = '../Context-Aware-Crowd-Counting_data/UCF-QNRF_ECCV18/'+dir+'/data.json'

        img_list = []

        for img_path in glob.glob(join(img_folder,'*.jpg')):
            img_list.append(img_path)

        with open(output_json,'w') as f:
            json.dump(img_list,f)
